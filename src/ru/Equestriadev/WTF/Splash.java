package ru.Equestriadev.WTF;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class Splash extends Activity {

	public static final String PREFS_NAME = "MyPrefsFile";
	SharedPreferences settings;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		settings = getSharedPreferences(PREFS_NAME, 0);
		TextView view =(TextView)findViewById(R.id.textView1);
		view.setTypeface(Typeface.createFromAsset(getAssets(),"Roboto-Black.ttf"));
	
		if(settings.getString("JSON File", null)==null&&!isConnected())
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("������ �������� :(")
					.setMessage("���� �� ����� � ��� ��� ������������� ����� � ���������!")
					.setCancelable(false)
					.setNegativeButton("�����",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									dialog.cancel();
									android.os.Process.killProcess(android.os.Process.myPid());
				                    System.exit(1);
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
		}
		
		else if(!isConnected())
		{
			chk();		
		}
		else
		{
			new HttpAsyncTask()
			.execute("https://spreadsheets.google.com/feeds/list/1uIS0_XiC_-hKgwYy43__g83YUReSROdV7TawKszrgGY/od6/public/values?alt=json");
		}
			
	}
	
	public static String GET(String url) {
		InputStream inputStream = null;
		String result = "";
		try {

			// create HttpClient
			HttpClient httpclient = new DefaultHttpClient();

			// make GET request to the given URL
			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

			// receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();

			// convert inputstream to string
			if (inputStream != null)
				result = convertInputStreamToString(inputStream);
			else
				result = "Did not work!";

		} catch (Exception e) {
			Log.d("InputStream", e.getLocalizedMessage());
		}

		return result;
	}
	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}
	private class HttpAsyncTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {

			return GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("JSON File", result);
			boolean com;
			com = editor.commit();
			if(com)
			{
			Toast.makeText(getBaseContext(), "�����!", Toast.LENGTH_LONG).show();
			chk();
			}
			else
			{
				chk();
				Toast.makeText(getBaseContext(), "�������� ������������� �����!", Toast.LENGTH_LONG).show();
			}
		}

	}
	public boolean isConnected() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected())
			return true;
		else
			return false;
	}
	public void chk()
	{
		if(settings.getInt("Group", 0) == 0)
		{
			final SharedPreferences.Editor editor = settings.edit();
			final String[] mCatsName ={"������ 17", "������ 18"};
			final Intent intent = new Intent(Splash.this, MainActivity.class);;
	        AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder = new AlertDialog.Builder(this);
	        builder.setTitle("������ ������"); // ��������� ��� �������

	        builder.setItems(mCatsName, new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int item) {
	                // TODO Auto-generated method stub
	                switch (item) {
					case 0:
						editor.putInt("Group", 17);
						editor.commit();
					    startActivity(intent);
					    overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
						break;
					case 1:
						editor.putInt("Group", 18);
						editor.commit();
					    startActivity(intent);
					    overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
						break;
					default:
						Toast.makeText(getApplicationContext(), "����� �����", Toast.LENGTH_LONG).show();
						break;
					}
	            }
	        });
	        builder.setCancelable(true);
	AlertDialog alert = builder.create();
	alert.show();
		}
		else
		{
			Intent intent = new Intent(Splash.this, MainActivity.class);
		    startActivity(intent);
		    overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
		}
	}
}
