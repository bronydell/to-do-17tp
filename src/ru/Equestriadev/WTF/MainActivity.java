package ru.Equestriadev.WTF;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity{
	TextView etResponse;
	TextView tvIsConnected;
	ArrayAdapter<String> adapter;
	ListView list;
	ArrayList<String> catnames = new ArrayList<String>();
	String Colorite;
	public static final String PREFS_NAME = "MyPrefsFile";
	SharedPreferences myPrefs;
	String[] time;
	private final int IDD_LIST_CATS = 1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		 list = (ListView) findViewById(R.id.listView1);
	
		refresh(true);
	}

	public void refresh(Boolean offline) {
		myPrefs = getSharedPreferences("myPrefs", MODE_PRIVATE);

		// check if you are connected or not
		if (isConnected()&&!offline) {
			new HttpAsyncTask()
					.execute("https://spreadsheets.google.com/feeds/list/1uIS0_XiC_-hKgwYy43__g83YUReSROdV7TawKszrgGY/od6/public/values?alt=json");
		} else {
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			parse(settings.getString("JSON File", null));
		}
		
		

	}

	public static String GET(String url) {
		InputStream inputStream = null;
		String result = "";
		try {

			// create HttpClient
			HttpClient httpclient = new DefaultHttpClient();

			// make GET request to the given URL
			HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

			// receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();

			// convert inputstream to string
			if (inputStream != null)
				result = convertInputStreamToString(inputStream);
			else
				result = "Did not work!";

		} catch (Exception e) {
			Log.d("InputStream", e.getLocalizedMessage());
		}

		return result;
	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while ((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;

	}

	public boolean isConnected() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected())
			return true;
		else
			return false;
	}

	private class HttpAsyncTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... urls) {

			return GET(urls[0]);
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			Toast.makeText(getBaseContext(), "Done!", Toast.LENGTH_LONG).show();

			// �������� ���!
			System.out.println("Work, Bitch");
			parse(result);

		}
	}

	public void parse(String result) {
		catnames = new ArrayList<String>();
		try {
			JSONObject main = new JSONObject(result);
			JSONObject feed = main.getJSONObject("feed");
			JSONArray entry = feed.getJSONArray("entry");

			List<Map<String, String>> listOfMaps = new ArrayList<Map<String, String>>();
			time = new String[5];
			SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
			for (int i = 0; i < entry.length(); i++) {
				Map<String, String> Map = new HashMap<String, String>();
				JSONObject object = entry.getJSONObject(i);
				JSONObject name = object.getJSONObject("gsx$name");
				JSONObject intro = object.getJSONObject("gsx$valueee");
				if(settings.getInt("Group", 0)==17)
				intro = object.getJSONObject("gsx$valueee");
				else if(settings.getInt("Group", 0)==18)
					intro = object.getJSONObject("gsx$value18");
					
				JSONObject times = object.getJSONObject("gsx$time");
				if (times != null) {
					time[i] = times.getString("$t");
				}
				Map.put(name.getString("$t"), intro.getString("$t"));
				listOfMaps.add(Map);

			}
			for (Map<String, String> map : listOfMaps) {
				//System.out.println(map.get("Pair"));
				if (map.get("Pair") != null) {
					catnames.add(map.get("Pair"));
				}
				if(map.get("Update")!=null)
				{
					TextView view = (TextView)findViewById(R.id.textView1);
					view.setText("��������� ����������: "+map.get("Update"));
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplicationContext(), "Error:" + e.toString(),
					Toast.LENGTH_SHORT).show();
		}
	
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("JSON File", result);
		editor.commit();
		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, catnames);
		list.setAdapter(adapter);
		list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> arg0, View v,
					int index, long arg3) {
				Toast.makeText(getApplicationContext(), time[index],
						Toast.LENGTH_SHORT).show();
						return false;

		}
			}
		);
		
		list.setOnItemClickListener(new OnItemClickListener(){


		@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				FromTwo to = new FromTwo();
				try {
					SimpleDateFormat s = new SimpleDateFormat("HH:mm");
					String format = s.format(new Date());
					String times[] = time[position].split(" - ");
					Toast.makeText(getApplicationContext(), to.Sub(format,times[0],times[1]),
							Toast.LENGTH_SHORT).show();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu items for use in the action bar
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle presses on the action bar items
		switch (item.getItemId()) {
		case R.id.action_search:
			if(isConnected())
				refresh(true);
			else
				refresh(false);
			return true;
		case R.id.action_about:
			showDialog(IDD_LIST_CATS);
			return true;
		case R.id.action_setup:
			ChangeGroup();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	@Override
	protected Dialog onCreateDialog(int id) {
	    switch (id) {
	    case IDD_LIST_CATS:
	        
	        final String[] mCatsName ={"����� � ��", "����� � Google+", "����� � Twitter", "�������� � �������� ��� ���������� ���-�� ���������� ;)","�������� ��� �� BitBucket"};
	        AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder = new AlertDialog.Builder(this);
	        builder.setTitle("�����"); // ��������� ��� �������

	        builder.setItems(mCatsName, new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int item) {
	                // TODO Auto-generated method stub
	                switch (item) {
					case 0:
						Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://vk.com/bronydell"));
						startActivity(browserIntent);
						break;
					case 1:
						Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/u/0/+RostislawNikishinorange"));
						startActivity(browserIntent1);
						break;
					case 2:
						Intent browserIntent11 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/bronydell"));
						startActivity(browserIntent11);
						break;
					case 3:
						Intent i = new Intent(Intent.ACTION_SEND);
						i.setType("message/rfc822");
						i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"littleponyapps@gmail.com"});
						i.putExtra(Intent.EXTRA_SUBJECT, "���");
						i.putExtra(Intent.EXTRA_TEXT   , "����� ��� ���");
						try {
						    startActivity(Intent.createChooser(i, "��������� ������..."));
						} catch (android.content.ActivityNotFoundException ex) {
						    Toast.makeText(getApplicationContext(), "�� ���������� �������� ������.", Toast.LENGTH_SHORT).show();
						}
						break;
					case 4:
						Intent browserIntent111 = new Intent(Intent.ACTION_VIEW, Uri.parse("https://bitbucket.org/bronydell/to-do-17tp/overview"));
						startActivity(browserIntent111);
						break;
					default:
						Toast.makeText(getApplicationContext(), "����� �����", Toast.LENGTH_LONG).show();
						break;
					}
	            }
	        });
	        builder.setCancelable(true);
	        return builder.create();
	        
	    default:
	        return null;
	    }
}
	protected void ChangeGroup() {
		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
		final SharedPreferences.Editor editor = settings.edit();
		final String[] mCatsName ={"������ 17", "������ 18"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder = new AlertDialog.Builder(this);
        builder.setTitle("������ ������"); // ��������� ��� �������
        
        builder.setItems(mCatsName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                // TODO Auto-generated method stub
                switch (item) {
				case 0:
					editor.putInt("Group", 17);
					editor.commit();
					 dialog.cancel();
					    refresh(true);
					break;
				case 1:
					editor.putInt("Group", 18);
					editor.commit();
				    dialog.cancel();
				    refresh(true);
					break;
				default:
					Toast.makeText(getApplicationContext(), "����� �����", Toast.LENGTH_LONG).show();
					break;
				}
            }
        });
        builder.setCancelable(true);
AlertDialog alert = builder.create();
alert.show();
	}
	 @Override
	    public void onBackPressed() {
		 moveTaskToBack(true);
	       return;
	    }
	 @Override
	 protected void onPause()
	 {
	     super.onPause();
	     overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
	 }
	 @Override
	 protected void onStart()
	 {
	     super.onStart();
	     overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
	 }
}
